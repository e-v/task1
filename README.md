# E&V Task 1

Deviations from the task set:
### Task Steps: Page 1
No error message will be displayed since we are dealing with security relevant information. Only status code `401` will indicate an invalid login attempt.

### Task Steps: Page 3
The endpoint is located under `/task/properties/{propertyId}` since this is more REST compliant.

## Postman Resources
Postman collection & environment export to be run with Postman / Newman can be found under `assets/postman`
