package me.clausius.ev.task;

import lombok.extern.slf4j.Slf4j;
import me.clausius.ev.task.persistence.repository.PropertyRepository;
import me.clausius.ev.task.persistence.repository.ViewRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
public class ImporterTest extends AbstractIntegrationTest {

    private final PropertyRepository propertyRepository;
    private final ViewRepository viewRepository;

    @Value("${import.properties}")
    private String propertiesImportFile;

    @Value("${import.views}")
    private String viewsImportFile;

    @Autowired
    public ImporterTest(
            PropertyRepository propertyRepository,
            ViewRepository viewRepository
    ) {
        this.propertyRepository = propertyRepository;
        this.viewRepository = viewRepository;
    }

    @Test
    void testPropertiesImport() {
        testImport(propertiesImportFile, propertyRepository);
    }

    @Test
    void testViewsImport() {
        testImport(viewsImportFile, viewRepository);
    }

    private void testImport(
            final String filePath,
            @SuppressWarnings("rawtypes") final CrudRepository repository
    ) {
        ClassLoader classLoader = getClass().getClassLoader();
        final URL url = classLoader.getResource(filePath);
        if (url != null) {
            final Path file = Paths.get(url.getFile());
            try (Stream<String> lines = Files.lines(file)) {
                assertTrue(lines.count() <= repository.count(), "Failed to import all data from import file!");
            } catch (IOException ex) {
                fail("Import file does not exist!");
            }
        }
    }

}
