package me.clausius.ev.task;

import me.clausius.ev.task.controller.dto.PropertyViewDTO;
import me.clausius.ev.task.persistence.repository.PropertyRepository;
import me.clausius.ev.task.persistence.repository.ViewRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

public class ApiTest extends AbstractIntegrationTest {

    @LocalServerPort
    private int port;

    private final TestRestTemplate restTemplate;

    private final PropertyRepository propertyRepository;

    private final ViewRepository viewRepository;

    @Autowired
    public ApiTest(
            final TestRestTemplate restTemplate,
            final PropertyRepository propertyRepository,
            final ViewRepository viewRepository) {
        this.restTemplate = restTemplate;
        this.propertyRepository = propertyRepository;
        this.viewRepository = viewRepository;
    }

    @Test
    public void testAddViewRecord() {

        final String uri = "http://localhost:{port}/task/properties/{propertyId}?username={username}";

        final Map<String, String> uriParam = new HashMap<>();
        uriParam.put("port", String.valueOf(this.port));
        uriParam.put("username", "a.clausius@gmail.com");

        long initialViewRecordCount = viewRepository.count();

        StreamSupport.stream(propertyRepository.findAll().spliterator(), true)
                .map(property -> {
                    uriParam.put("propertyId", property.getPropertyId());
                    return uriParam;
                })
                .map(params -> this.restTemplate.getForObject(
                        uri,
                        PropertyViewDTO.class,
                        uriParam
                ))
                .forEach(ApiTest::testResult);
        assertEquals(
                viewRepository.count() - initialViewRecordCount, propertyRepository.count(),
                "There should be a new view record for each property"
        );

    }

    private static void testResult(PropertyViewDTO dto) {
        assertNotNull(dto, "Property should not be null");
        // TODO
    }

}
