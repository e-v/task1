package me.clausius.ev.task.controller;

import lombok.extern.slf4j.Slf4j;
import me.clausius.ev.task.auth.domain.Token;
import me.clausius.ev.task.auth.domain.User;
import me.clausius.ev.task.auth.factory.TokenFactory;
import me.clausius.ev.task.auth.provider.AuthProvider;
import me.clausius.ev.task.auth.provider.BasicAuthProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class LoginController {

    private final AuthProvider<String> basicAuthProvider;

    private final TokenFactory tokenFactory;

    public LoginController(
            final BasicAuthProvider basicAuthProvider,
            final TokenFactory tokenFactory
    ) {
        this.basicAuthProvider = basicAuthProvider;
        this.tokenFactory = tokenFactory;
    }

    @GetMapping("/task/login")
    public ResponseEntity<Token> login(
            @RequestHeader(value = HttpHeaders.AUTHORIZATION, required = false) final String authorization
    ) {

        final User user = basicAuthProvider.authenticate(authorization);

        final Token token = tokenFactory.generateToken(user);
        return new ResponseEntity<>(token, HttpStatus.OK);

    }

}
