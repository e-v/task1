package me.clausius.ev.task.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.clausius.ev.task.domin.Property;

import java.util.List;

@Data
@NoArgsConstructor
public class PropertyViewDTO {

    private Property property;

    @JsonProperty("similar")
    private List<Property> similarProperties;

}
