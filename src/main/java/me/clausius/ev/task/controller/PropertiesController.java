package me.clausius.ev.task.controller;

import me.clausius.ev.task.controller.dto.PropertyViewDTO;
import me.clausius.ev.task.domin.Property;
import me.clausius.ev.task.domin.View;
import me.clausius.ev.task.persistence.repository.ViewRepository;
import me.clausius.ev.task.persistence.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PropertiesController {

    private final PropertyService propertyService;

    private final ViewRepository viewRepository;

    @Autowired
    public PropertiesController(
            final PropertyService propertyService,
            final ViewRepository viewRepository) {
        this.propertyService = propertyService;
        this.viewRepository = viewRepository;
    }

    @GetMapping("/task/properties")
    public ResponseEntity<Page<Property>> listProperties(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String sort,
            @RequestParam(required = false) String sortProperty
    ) {
        page = page != null ? page : 0;
        pageSize = pageSize != null ? pageSize : 10;
        final Sort.Direction direction = Sort.Direction.fromOptionalString(sort).orElse(Sort.Direction.ASC);
        final PropertyService.SortProperty property = PropertyService.SortProperty.fromValue(sortProperty);

        return new ResponseEntity<>(propertyService.findAll(page, pageSize, direction, property), HttpStatus.OK);
    }

    @GetMapping("/task/properties/{propertyId}")
    public ResponseEntity<PropertyViewDTO> propertyDetail(
            @PathVariable String propertyId,
            @RequestParam String username
    ) {
        final Property property = propertyService.findOne(propertyId);
        if (property == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        final View view = new View();
        view.setUser(username);
        view.setProperty(property);
        viewRepository.save(view);

        PropertyViewDTO dto = new PropertyViewDTO();
        dto.setProperty(property);
        dto.setSimilarProperties(propertyService.findSimilar(property));
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

}
