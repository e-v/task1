package me.clausius.ev.task.auth.exception;

//@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class AuthException extends RuntimeException {

    public static final AuthException NO_AUTH_DATA = create("No auth data provided");
    public static final AuthException INVALID_AUTH_DATA = create("Invalid auth data provided");

    private AuthException() {
    }

    private AuthException(final String message) {
        super(message);
    }

    public static AuthException create(String message) {
        return new AuthException(message);
    }

}
