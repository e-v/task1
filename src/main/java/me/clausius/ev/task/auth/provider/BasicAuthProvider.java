package me.clausius.ev.task.auth.provider;

import lombok.extern.slf4j.Slf4j;
import me.clausius.ev.task.auth.domain.User;
import me.clausius.ev.task.auth.exception.AuthException;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Set;

@Slf4j
@Component
public class BasicAuthProvider implements AuthProvider<String> {

    private final Validator validator;

    public BasicAuthProvider() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            this.validator = factory.getValidator();
        }
    }

    @Override
    public User authenticate(String authData) {
        if (authData == null || authData.isEmpty()) {
            throw AuthException.NO_AUTH_DATA;
        }
        final String[] parts = authData.split(" ");
        if (parts.length != 2 || !"Basic".equals(parts[0])) {
            throw AuthException.INVALID_AUTH_DATA;
        }
        final String decodedAuthData;
        try {
            byte[] decoded = Base64.getDecoder().decode(parts[1]);
            decodedAuthData = new String(decoded, StandardCharsets.UTF_8);
        } catch (IllegalArgumentException ex) {
            throw AuthException.INVALID_AUTH_DATA;
        }
        final String[] splitAuthData = decodedAuthData.split(":");
        if (splitAuthData.length != 2) {
            throw AuthException.INVALID_AUTH_DATA;
        }
        return validate(new User(splitAuthData[0], splitAuthData[1]));
    }

    private User validate(final User user) {
        final Set<ConstraintViolation<User>> violations = validator.validate(user);
        for (ConstraintViolation<User> violation : violations) {
            log.debug(violation.getMessage());
        }
        if (!violations.isEmpty()) {
            throw AuthException.INVALID_AUTH_DATA;
        }
        return user;
    }

}
