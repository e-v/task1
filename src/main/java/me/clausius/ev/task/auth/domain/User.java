package me.clausius.ev.task.auth.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.clausius.ev.task.auth.validation.Domain;

import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
public class User {

  //  @Email(message = "Invalid E-Mail provided")
    @Domain(domain = "test.com")
    @Size
    private String username;

    private String password;

}
