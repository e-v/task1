package me.clausius.ev.task.auth.validation;

import me.clausius.ev.task.auth.validation.validators.DomainValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = DomainValidator.class)
@Target({FIELD})
@Retention(RUNTIME)
@Documented
public @interface Domain {

    String message() default "End date must be after begin date "
            + "and both must be in the future, room number must be bigger than 0";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String domain() default "";

}
