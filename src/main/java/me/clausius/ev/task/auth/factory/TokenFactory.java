package me.clausius.ev.task.auth.factory;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import lombok.extern.slf4j.Slf4j;
import me.clausius.ev.task.auth.domain.Token;
import me.clausius.ev.task.auth.domain.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Slf4j
@Component
public class TokenFactory {

    private Algorithm algorithm = null;

    @Value("${token.issuer}")
    private String issuer;

    @Value("${token.access.ttl}")
    private Long accessTokenTTL;

    @Value("${token.refresh.ttl}")
    private Long refreshTokenTTL;

    public TokenFactory(@Value("${token.secret}") String secret) {
        try {
            this.algorithm = Algorithm.HMAC256(secret);
        } catch (IllegalArgumentException ex) {
            log.error("Failed in creating algorithm", ex);
        }
    }

    public Token generateToken(final User user) {
        final Instant now = ZonedDateTime.now().toInstant();
        return Token.builder()
                .accessToken(buildAccessToken(now, user.getUsername()))
                .refreshToken(buildRefreshToken(now, user.getUsername()))
                .build();
    }

    protected final String buildAccessToken(
            final Instant now,
            final String subject
    ) {
        return buildToken(now, accessTokenTTL, subject, "properties.list", "properties.details");
    }

    protected final String buildRefreshToken(
            final Instant now,
            final String subject
    ) {
        return buildToken(now, refreshTokenTTL, subject, "authorize");
    }

    private String buildToken(
            final Instant now,
            final Long ttl,
            final String subject,
            final String... permissions
    ) {
        return JWT.create()
                .withIssuer(this.issuer)
                .withSubject(subject)
                .withIssuedAt(now)
                .withNotBefore(now)
                .withExpiresAt(now.plus(ttl, ChronoUnit.SECONDS))
                .withClaim("permissions", List.of(permissions))
                .sign(algorithm);
    }

}
