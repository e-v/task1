package me.clausius.ev.task.auth.domain;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Token {

    private String accessToken;

    private String refreshToken;

}
