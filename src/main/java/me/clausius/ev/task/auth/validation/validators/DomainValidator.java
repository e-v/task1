package me.clausius.ev.task.auth.validation.validators;

import me.clausius.ev.task.auth.validation.Domain;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class DomainValidator implements ConstraintValidator<Domain, String> {

    private String domain;

    @Override
    public void initialize(Domain constraintAnnotation) {
        this.domain = constraintAnnotation.domain();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !domain.equals(value.substring(value.indexOf("@") + 1));
    }

}
