package me.clausius.ev.task.auth.provider;

import me.clausius.ev.task.auth.domain.User;

public interface AuthProvider<T> {

    User authenticate(T authData);

}
