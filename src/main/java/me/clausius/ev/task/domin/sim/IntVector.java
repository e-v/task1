package me.clausius.ev.task.domin.sim;

import java.util.Collections;
import java.util.List;
import java.util.SortedSet;

public class IntVector<C> extends Vector<C, Integer> {

    public IntVector(SortedSet<C> coordinates, List<Integer> values) {
        super(
                coordinates != null ? coordinates : Collections.emptySortedSet(),
                values != null ? values : Collections.emptyList()
        );
    }

    @Override
    public Integer innerProduct(Vector<C, Integer> other) {
        int result = 0;
        for (int i = 0; i < coordinates.size(); i++) {
            result += this.values.get(i) * other.values.get(i);
        }
        return result;
    }

}
