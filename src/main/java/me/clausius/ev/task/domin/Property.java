package me.clausius.ev.task.domin;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Property {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, unique = true)
    private String propertyId;

    private String name;

    private String details;

    private BigDecimal price;

    private String image;

}
