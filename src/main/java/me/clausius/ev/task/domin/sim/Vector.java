package me.clausius.ev.task.domin.sim;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

public abstract class Vector<C, V extends Number> {

    protected final Set<C> coordinates;
    protected final LinkedList<V> values;

    public Vector(final SortedSet<C> coordinates, final List<V> values) {
        if (coordinates == null) {
            throw new IllegalArgumentException("Coordinates may not be empty");
        }
        if (!(coordinates.size() == values.size())) {
            throw new IllegalArgumentException("Invalid vales size, expected: " + coordinates.size() + ", found: " + values.size());
        }
        this.coordinates = coordinates;
        this.values = new LinkedList<>(values);
    }

    public abstract V innerProduct(Vector<C, V> other);

    public double norm() {
        return Math.sqrt(innerProduct(this).doubleValue());
    }

    public static <C, V extends Number> V innerProduct(Vector<C, V> v1, Vector<C, V> v2) {
        return v1.innerProduct(v2);
    }

}
