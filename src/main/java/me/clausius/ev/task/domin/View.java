package me.clausius.ev.task.domin;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
public class View {

    @Id
    @GeneratedValue
    private Long id;

    private String user;

    @OneToOne
    private Property property;

}
