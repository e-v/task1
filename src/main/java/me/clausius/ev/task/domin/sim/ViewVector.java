package me.clausius.ev.task.domin.sim;

import java.util.List;
import java.util.SortedSet;

public class ViewVector extends IntVector<String>{

    public ViewVector(SortedSet<String> users, List<Integer> views) {
        super(users, views);
    }

}
