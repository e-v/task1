package me.clausius.ev.task.domin.sim.mapper;

import me.clausius.ev.task.domin.Property;

public interface SimilarityMapper {

    double similarity(Property p1, Property p2);

}
