package me.clausius.ev.task.domin.sim.mapper;

import me.clausius.ev.task.domin.Property;
import me.clausius.ev.task.domin.sim.Vector;
import me.clausius.ev.task.domin.sim.ViewVector;
import me.clausius.ev.task.persistence.repository.ViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.SortedSet;
import java.util.TreeSet;

@Component
public class CosineSimilarityMapper implements SimilarityMapper {

    private final ViewRepository viewRepository;
    private final SortedSet<String> users;

    @Autowired
    public CosineSimilarityMapper(ViewRepository viewRepository) {
        this.viewRepository = viewRepository;
        this.users = new TreeSet<>(viewRepository.findDistinctUsers());
    }

    @Override
    public double similarity(Property p1, Property p2) {
        final ViewVector v1 = getVector(p1);
        final ViewVector v2 = getVector(p2);
        return Vector.innerProduct(v1, v2) / (v1.norm() * v2.norm());
    }

    private ViewVector getVector(Property p1) {
        return new ViewVector(
                users,
                users.stream()
                        .map(user -> viewRepository.countByUserAndProperty(user, p1))
                        .toList()
        );
    }

}
