package me.clausius.ev.task.batch.dto.mapper;

import me.clausius.ev.task.batch.dto.PropertyDTO;
import me.clausius.ev.task.domin.Property;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface PropertyDTOMapper {

    @Mapping(source = "price", target = "price", numberFormat = "#,##0.###")
    @Mapping(source = "id", target = "propertyId")
    @Mapping(target = "id", ignore = true)
    Property toProperty(final PropertyDTO dto);

}
