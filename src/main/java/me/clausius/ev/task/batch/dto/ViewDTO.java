package me.clausius.ev.task.batch.dto;

import lombok.Data;

@Data
public class ViewDTO {

    private String property;

    private String user;

}
