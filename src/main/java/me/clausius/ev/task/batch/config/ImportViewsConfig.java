package me.clausius.ev.task.batch.config;

import me.clausius.ev.task.batch.dto.ViewDTO;
import me.clausius.ev.task.batch.dto.mapper.ViewDTOMapper;
import me.clausius.ev.task.domin.View;
import me.clausius.ev.task.persistence.repository.ViewRepository;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.repository.CrudRepository;

import java.util.Arrays;

@Configuration
@EnableBatchProcessing
public class ImportViewsConfig {

    public final JobBuilderFactory jobBuilderFactory;

    public final StepBuilderFactory stepBuilderFactory;

    @Value("${import.views}")
    private String viewsImportFile;

    public ImportViewsConfig(
            final JobBuilderFactory jobBuilderFactory,
            final StepBuilderFactory stepBuilderFactory
    ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public ItemReader<ViewDTO> viewsReader() {
        return new FlatFileItemReaderBuilder<ViewDTO>()
                .name("viewItemReader")
                .resource(new ClassPathResource(viewsImportFile))
                .delimited()
                .delimiter(";")
                .names("property", "view")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<ViewDTO>() {{
                    setTargetType(ViewDTO.class);
                }})
                .targetType(ViewDTO.class)
                .build();
    }

    @Bean
    public RepositoryItemWriter<View> viewsWriter(CrudRepository<View, Long> repository) {
        RepositoryItemWriter<View> itemWriter = new RepositoryItemWriter<>();
        itemWriter.setRepository(repository);
        return itemWriter;
    }

    @Bean
    public Step step2(RepositoryItemWriter<View> writer,
                      ItemProcessor<ViewDTO, View> viewsCompositeItemProcessor) {
        return stepBuilderFactory.get("import views data")
                .<ViewDTO, View>chunk(10)
                .reader(viewsReader())
                .processor(viewsCompositeItemProcessor)
                .writer(writer)
                .build();
    }

    @Bean
    public ItemProcessor<ViewDTO, View> viewsMappingProcessor(ViewDTOMapper mapper) {
        return mapper::toView;
    }

    @Bean
    public ItemProcessor<View, View> viewsFilterProcessor(final ViewRepository repository) {
        return item -> {
            if (repository.existsByUserAndProperty(item.getUser(), item.getProperty()))
                return null;
            return item;
        };
    }

    @Bean(name = "viewsCompositeItemProcessor")
    public ItemProcessor<ViewDTO, View> viewsCompositeItemProcessor(
            final ViewRepository repository,
            final ViewDTOMapper mapper) {
        CompositeItemProcessor<ViewDTO, View> compositeItemProcessor = new CompositeItemProcessor<>();
        compositeItemProcessor.setDelegates(Arrays.asList(viewsMappingProcessor(mapper), viewsFilterProcessor(repository)));
        return compositeItemProcessor;
    }

}
