package me.clausius.ev.task.batch.dto;

import lombok.Data;

@Data
public class PropertyDTO {

    private String id;

    private String name;

    private String details;

    private String price;

    private String image;

}
