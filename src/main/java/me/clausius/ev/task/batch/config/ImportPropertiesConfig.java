package me.clausius.ev.task.batch.config;

import me.clausius.ev.task.batch.dto.PropertyDTO;
import me.clausius.ev.task.batch.dto.mapper.PropertyDTOMapper;
import me.clausius.ev.task.domin.Property;
import me.clausius.ev.task.persistence.repository.PropertyRepository;
import org.mapstruct.factory.Mappers;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.repository.CrudRepository;

import java.util.Arrays;

@Configuration
@EnableBatchProcessing
public class ImportPropertiesConfig {

    public final JobBuilderFactory jobBuilderFactory;

    public final StepBuilderFactory stepBuilderFactory;

    @Value("${import.properties}")
    private String propertiesImportFile;

    public ImportPropertiesConfig(
            final JobBuilderFactory jobBuilderFactory,
            final StepBuilderFactory stepBuilderFactory
    ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public ItemReader<PropertyDTO> propertiesReader() {
        return new FlatFileItemReaderBuilder<PropertyDTO>()
                .name("propertyItemReader")
                .resource(new ClassPathResource(propertiesImportFile))
                .delimited()
                .delimiter(";")
                .names("id", "name", "details", "price", "image")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<PropertyDTO>() {{
                    setTargetType(PropertyDTO.class);
                }})
                .targetType(PropertyDTO.class)
                .build();
    }

    @Bean
    public RepositoryItemWriter<Property> propertiesWriter(CrudRepository<Property, Long> repository) {
        RepositoryItemWriter<Property> itemWriter = new RepositoryItemWriter<>();
        itemWriter.setRepository(repository);
        return itemWriter;
    }


    @Bean
    public Step step1(RepositoryItemWriter<Property> writer,
                      ItemProcessor<PropertyDTO, Property> propertiesCompositeItemProcessor) {
        return stepBuilderFactory.get("import properties data")
                .<PropertyDTO, Property>chunk(10)
                .reader(propertiesReader())
                .processor(propertiesCompositeItemProcessor)
                .writer(writer)
                .build();
    }

    @Bean
    public ItemProcessor<PropertyDTO, Property> propertiesMappingProcessor() {
        return item -> Mappers.getMapper(PropertyDTOMapper.class).toProperty(item);
    }

    @Bean
    public ItemProcessor<Property, Property> propertiesFilterProcessor(final PropertyRepository repository) {
        return item -> {
            if (repository.existsByPropertyId(item.getPropertyId()))
                return null;
            return item;
        };
    }

    @Bean(name = "propertiesCompositeItemProcessor")
    public ItemProcessor<PropertyDTO, Property> propertiesCompositeItemProcessor(final PropertyRepository repository) {
        CompositeItemProcessor<PropertyDTO, Property> compositeItemProcessor = new CompositeItemProcessor<>();
        compositeItemProcessor.setDelegates(Arrays.asList(propertiesMappingProcessor(), propertiesFilterProcessor(repository)));
        return compositeItemProcessor;
    }

}
