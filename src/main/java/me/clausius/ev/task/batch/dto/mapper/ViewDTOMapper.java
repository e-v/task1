package me.clausius.ev.task.batch.dto.mapper;

import me.clausius.ev.task.batch.dto.ViewDTO;
import me.clausius.ev.task.domin.View;
import me.clausius.ev.task.persistence.repository.PropertyRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;


@Mapper(componentModel = "spring", uses = PropertyRepository.class)
public abstract class ViewDTOMapper {

    @Autowired
    protected PropertyRepository propertyRepository;

    @Mapping(source = "property", target = "property")
    public abstract View toView(final ViewDTO dto);

}
