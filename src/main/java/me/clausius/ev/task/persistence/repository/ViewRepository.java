package me.clausius.ev.task.persistence.repository;

import me.clausius.ev.task.domin.Property;
import me.clausius.ev.task.domin.View;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ViewRepository extends CrudRepository<View, Long> {

    Boolean existsByUserAndProperty(String user, Property property);

    List<View> findAllByProperty(Property property);

    @Query("SELECT DISTINCT user AS user FROM View")
    List<String> findDistinctUsers();

    Integer countByUserAndProperty(String user, Property property);

}
