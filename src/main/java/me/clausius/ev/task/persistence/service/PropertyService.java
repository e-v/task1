package me.clausius.ev.task.persistence.service;

import me.clausius.ev.task.domin.Property;
import me.clausius.ev.task.domin.sim.mapper.SimilarityMapper;
import me.clausius.ev.task.persistence.repository.PropertyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.StreamSupport;

@Service
public class PropertyService {

    private final PropertyRepository propertyRepository;

    public final SimilarityMapper similarityMapper;

    @Autowired
    public PropertyService(
            final PropertyRepository propertyRepository,
            final SimilarityMapper similarityMapper) {
        this.propertyRepository = propertyRepository;
        this.similarityMapper = similarityMapper;
    }

    public Property save(final Property property) {
        if (!this.propertyRepository.existsByPropertyId(property.getPropertyId())) {
            return this.propertyRepository.save(property);
        } else return property;
    }

    public Property findOne(final String propertyId) {
        return propertyRepository.findByPropertyId(propertyId);
    }

    public Page<Property> findAll(
            final int page,
            final int pageSize
    ) {
        return findAll(page, pageSize, Sort.Direction.ASC, SortProperty.PROPERTY_ID);
    }

    public Page<Property> findAll(
            final int page,
            final int pageSize,
            final Sort.Direction direction,
            final SortProperty sortProperty
    ) {
        return propertyRepository.findAll(PageRequest.of(page, pageSize, direction, sortProperty.value));
    }

    public List<Property> findSimilar(
            final Property property
    ) {
        return StreamSupport.stream(propertyRepository.findAll().spliterator(), false)
                .filter(other -> !other.getPropertyId().equals(property.getPropertyId()))
                .map(referenceProperty -> Pair.of(referenceProperty, similarityMapper.similarity(referenceProperty, property)))
                .sorted(Comparator.comparing(Pair::getSecond))
                .limit(5)
                .map(Pair::getFirst)
                .toList();
    }

    public enum SortProperty {

        PROPERTY_ID("propertyId");

        private final String value;

        SortProperty(String value) {
            this.value = value;
        }

        public static SortProperty fromValue(final String value) {
            PropertyService.SortProperty property;
            try {
                property = PropertyService.SortProperty.valueOf(value);
            } catch (Exception ex) {
                property = PropertyService.SortProperty.PROPERTY_ID;
            }
            return property;
        }

    }

}
