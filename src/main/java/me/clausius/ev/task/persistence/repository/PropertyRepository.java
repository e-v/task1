package me.clausius.ev.task.persistence.repository;

import me.clausius.ev.task.domin.Property;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface PropertyRepository extends CrudRepository<Property, Long> {

    Property findByPropertyId(String id);

    Page<Property> findAll(Pageable pageable);

    Boolean existsByPropertyId(String propertyId);

}
